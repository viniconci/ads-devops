<?php 
session_start();
ob_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8"/>
    <title>Cadastro de usuário</title>
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="estilo.css" media="screen" />
  </head>
  <body>
  <header>
     <nav>
	 <figure>
             <img alt="Logo" src="img/logo.png">
	 </figure>
     </nav>
  </header>
  <main>
    <div class="container">
        <div class="container-content">
        <h1>Insira seus dados para efetuar o cadastro!</h1>
        <form id="cadastro" action="process-cadastro.php" method="post">
	          <label for="nome">Nome</label><br>
                  <input type="text" name="nome" id="nome" required="required" placeholder="Michael Scott" /><br>
	          <label for="email">E-mail</label><br>
		  <input type="email" name="email" id="email" required="required" placeholder="michael@dundermifflin.com.br" /><br>
		  <label for="senha">Senha</label><br>
		  <input type="password" name="senha" id="senha" required="required" /><br>
		  <label for="telefone">Telefone</label><br>
		  <input type="tel" name="telefone" id="telefone" required="required" placeholder="41 99904 5026" /><br>
		  <label for="cpf">CPF</label><br>
		  <input type="text" name="cpf" id="cpf" required="required" placeholder="000 000 000 00"/><br>
		  <label for="estado">Estado</label><br>
		  <input type="text" name="estado" id="estado" required="required" placeholder="Pennsylvania"/><br>
		  <label for="cidade">Cidade</label><br>
		  <input type="text" name="cidade" id="cidade" required="required" placeholder="Scranton" /><br>
		  <label for="rua">Rua</label><br>
		  <input type="text" name="rua" id="rua" required="required" placeholder="Slough Avenue" /><br>
		  <label for="numero">Número </label><br>
		  <input type="text" name="numero" id="numero" min="1" required="required" placeholder="1725" /><br>
		  <label for="comp">Complemento</label><br>
		  <input type="text" name="comp" id="comp" placeholder="Scranton Business Park" /><br><br>
		  <input type="reset" name="limpar" id="limpar" value="Limpar" />
		  <input type="submit" name="enviar" value="Enviar" /><br>
		  <p>  
		      Já tem conta?
		      <a href="index.php"> Fazer Login </a>
		  </p>
		</form>
	  </div>
	</div>  
  </main>
  </body>
</html>