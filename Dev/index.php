<?php 
 session_start(); 
 ob_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8"/>
    <title>Acesso a biblioteca</title>
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="estilo.css" media="screen" />
  </head>
  <body>
  <main>
    <div class="login">
      <div class="login-left">
	    <img src="img/biblioteca.jpg" />
	  </div>
      <div class="login-right">
        <div>
		<h1>Fazer login</h1>
            <form id="cadastro" action="process-login.php" method="post">
	      <label for="email">E-mail</label><br>
              <input type="email" name="email" id="email" required="required" /><br>
              <label for="senha">Senha</label><br>
              <input type="password" name="senha" id="senha" required="required" /><br>
	      <input type="submit" name="entrar" value="Entrar" />
	      <p class="link">
                  Ainda não tem conta?<br>
                  <a href="cadastro-user.php">Cadastre-se aqui!</a>
              </p>
              <?php
                  if(isset($_SESSION['msg'])){
                      echo $_SESSION['msg'];
                      unset($_SESSION['msg']);
                  }
                  if(isset($_SESSION['msgcad'])){
                  echo $_SESSION['msgcad'];
                  unset($_SESSION['msgcad']);
                  }
              ?>
	    </form>
		</div>
	  </div>
    </div>
  </main>
  </body>

</html>