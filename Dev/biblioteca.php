<?php
session_start();
ob_start();
if(!empty($_SESSION['id'])){
    
}
else{
    $_SESSION['msg'] =  "<p>Faça o login!</p>";
    header("Location: index.php");
}
include_once("conect.php");
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8"/>
    <title>Biblioteca</title>
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="estilo.css" media="screen" />
  </head>
  <body>
  <header>
     <nav>
	 <figure>
             <a href="biblioteca.php"><img alt="Logo" src="img/logo.png"></a>
	 </figure>
         <div class="dropdown">
             <button class="dropbtn"><?php echo $_SESSION['nome'] . " &#9787"; ?></button>
                 <div class="dropdown-content">
                     <a href="edit-user.php">Editar perfil</a>
                     <a href="logout.php">Logout</a>
                 </div>
         </div>
     </nav>
  </header>
  <main>
    <div class="container">
      <div class="container-content">
	      <h1>Biblioteca</h1>
              <h2>Empreste livros agora mesmo!</h2>
              <hr>
              <a style= color:#78E84F; href="cadastro-livro.php">+ cadastrar novo livro</a>
              <hr>
              <?php
              if(isset($_SESSION['msg'])){
                      echo $_SESSION['msg'];
                      unset($_SESSION['msg']);
                  }
              $result_books = "SELECT * FROM books";
              $resutado_books = mysqli_query ($conn, $result_books);
              while($row_book = mysqli_fetch_assoc ($resutado_books)){
                  echo "<h3>" . $row_book['nome'] . "</h3>";
                  echo "<p>" . $row_book['autor'] . "</p>";
                  echo "<p>Ano: " . $row_book['ano'] . "</p>";
                  echo "<p>Páginas: " . $row_book['paginas'] . "</p>";
                  echo "<p>Editora: " . $row_book['editora'] . "</p>";
                  echo "<a style= color:#63b8ff; href='edit-livro.php?id=" . $row_book['id'] . "'>Editar &emsp;</a>";
                  echo "<a style= color:#FF827D; href='process-delete-livro.php?id=" . $row_book['id'] . "'>Apagar</a><br><hr>";
              }
              ?>
	  </div>  
    </div>
  </main>
  </body>
</html>