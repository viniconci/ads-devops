<?php 
 session_start();  
include_once("conect.php");
$entrar = filter_input (INPUT_POST, 'entrar', FILTER_SANITIZE_STRING);
if($entrar){
    $email = filter_input (INPUT_POST, 'email', FILTER_SANITIZE_STRING);
    $senha = filter_input (INPUT_POST, 'senha', FILTER_SANITIZE_STRING);
    if((!empty($email)) AND (!empty($senha))){
        $result_user = "SELECT id, nome, email, senha FROM users WHERE email='$email' LIMIT 1";
        $resultado_user = mysqli_query($conn, $result_user);
        if($resultado_user){
            $row_user = mysqli_fetch_assoc($resultado_user);
            if(password_verify($senha,  $row_user['senha'])){
                $_SESSION['id'] =  $row_user['id'];
                $_SESSION['nome'] =  $row_user['nome'];
                $_SESSION['email'] =  $row_user['email'];
                header("Location: biblioteca.php");
            }
            else{
            $_SESSION['msg']="<p>Senha incorreta!</p>";
            header("Location: index.php");
            }
        }
        else{
        $_SESSION['msg']="<p>Preencha todos os campos!</p>";
        header("Location: index.php");
        }
    }
    else{
        $_SESSION['msg']="<p>Login e/ou senha incorretos!</p>";
        header("Location: index.php");
    }
}
else{
    $_SESSION['msg']="<p>Página não encontrada</p>";
    header("Location: index.php");
}