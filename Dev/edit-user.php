<?php 
session_start();
ob_start();
if(!empty($_SESSION['id'])){
    
}
else{
    $_SESSION['msg'] =  "<p>Faça o login!</p>";
    header("Location: index.php");
}
include_once("conect.php");
$id = filter_input (INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
$result_user = "SELECT * FROM users WHERE id='$id'";
$resutadouser = mysqli_query ($conn, $result_user);
$row_user = mysqli_fetch_assoc ($resutadouser);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8"/>
    <title>Editar perfil</title>
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="estilo.css" media="screen" />
  </head>
  <body>
  <header>
     <nav>
	 <figure>
             <a href="biblioteca.php"><img alt="Logo" src="img/logo.png"></a>
	 </figure>
         <div class="dropdown">
             <button class="dropbtn"><?php echo $_SESSION['nome'] . " &#9787"; ?></button>
                 <div class="dropdown-content">
                     <a href="edit-user.php">Editar perfil</a>
                     <a href="logout.php">Logout</a>
                 </div>
         </div>
     </nav>
  </header>
  <main>
    <div class="container">
        <div class="container-content">
        <h1>Olar, <?php echo $_SESSION['nome'];?>! &#9787</h1>
        <form id="cadastro" action="process-edit-user.php" method="post">
                  <input type="hidden" name="id" value="<?php echo $_SESSION ['id']; ?>" /><br>
	          <label for="nome">Nome</label><br>
                  <input type="text" name="nome" id="nome" required="required" value="<?php echo $_SESSION ['nome']; ?>" /><br>
	          <label for="email">E-mail</label><br>
		  <input type="email" name="email" id="email" required="required" value="<?php echo $_SESSION ['email']; ?>" /><br>
		  <label for="senha">Senha</label><br>
		  <input type="password" name="senha" id="senha" required="required" value="<?php echo $_SESSION ['senha']; ?>" /><br>
		  <label for="telefone">Telefone</label><br>
		  <input type="tel" name="telefone" id="telefone" required="required" value="<?php echo $_SESSION ['telefone']; ?>" /><br>
		  <label for="cpf">CPF</label><br>
		  <input type="text" name="cpf" id="cpf" required="required" value="<?php echo $_SESSION ['cpf']; ?>" /><br>
		  <label for="estado">Estado</label><br>
		  <input type="text" name="estado" id="estado" required="required" value="<?php echo $_SESSION ['estado']; ?>" /><br>
		  <label for="cidade">Cidade</label><br>
		  <input type="text" name="cidade" id="cidade" required="required" value="<?php echo $_SESSION ['cidade']; ?>" /><br>
		  <label for="rua">Rua</label><br>
		  <input type="text" name="rua" id="rua" required="required" value="<?php echo $_SESSION ['rua']; ?>" /><br>
		  <label for="numero">Número </label><br>
		  <input type="text" name="numero" id="numero" required="required" value="<?php echo $_SESSION ['numero']; ?>" /><br>
		  <label for="comp">Complemento</label><br>
		  <input type="text" name="comp" id="comp" value="<?php echo $_SESSION ['comp']; ?>" /><br><br>
                  <input type="reset" name="limpar" id="limpar" value="Limpar" />
		  <input type="submit" name="enviar" value="Salvar alterações" /><br>
        </form>
	  </div>
	</div>  
  </main>
  </body>
</html>