<?php 
session_start();
ob_start();
if(!empty($_SESSION['id'])){
    
}
else{
    $_SESSION['msg'] =  "<p>Faça o login!</p>";
    header("Location: index.php");
}
include_once("conect.php");
$id = filter_input (INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
$result_book = "SELECT * FROM books WHERE id = '$id'";
$resutadobook = mysqli_query ($conn, $result_book);
$row_book = mysqli_fetch_assoc ($resutadobook);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8"/>
    <title>Editar livro</title>
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="estilo.css" media="screen" />
  </head>
  <body>
  <header>
     <nav>
	 <figure>
             <a href="biblioteca.php"><img alt="Logo" src="img/logo.png"></a>
	 </figure>
         <div class="dropdown">
             <button class="dropbtn"><?php echo $_SESSION['nome'] . " &#9787"; ?></button>
                 <div class="dropdown-content">
                     <a href="edit-user.php">Editar perfil</a>
                     <a href="logout.php">Logout</a>
                 </div>
         </div>
     </nav>
  </header>
  <main>
    <div class="container">
        <div class="container-content">
        <h1>Edite os dados do livro!</h1>
        <form id="cadastro" action="process-edit-livro.php" method="post">
                  <input type="hidden" name="id" value="<?php echo $row_book ['id']; ?>" /><br>
	          <label for="nome">Nome</label><br>
                  <input type="text" name="nome" id="nome" required="required" value="<?php echo $row_book ['nome']; ?>" /><br>
	          <label for="autor">Autor</label><br>
		  <input type="text" name="autor" id="autor" required="required" value="<?php echo $row_book ['autor']; ?>" /><br>
                  <label for="ano">Ano</label><br>
		  <input type="text" name="ano" id="ano" required="required" value="<?php echo $row_book ['ano']; ?>" /><br>
		  <label for="paginas">Páginas</label><br>
		  <input type="text" name="paginas" id="paginas" required="required" value="<?php echo $row_book ['paginas']; ?>" /><br>
                  <label for="editora">Editora</label><br>
		  <input type="text" name="editora" id="editora" required="required" value="<?php echo $row_book ['editora']; ?>" /><br>
		  <input type="reset" name="limpar" id="limpar" value="Limpar" />
		  <input type="submit" name="enviar" value="Salvar alterações" /><br>
        </form>
	  </div>
	</div>  
  </main>
  </body>
</html>