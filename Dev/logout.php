<?php
session_start();

unset($_SESSION['id'], $_SESSION['nome'], $_SESSION['email']);

$_SESSION['msg'] =  "<p>Deslogado</p>";
header("Location: index.php");
