<?php 
session_start();
ob_start();
if(!empty($_SESSION['id'])){
    
}
else{
    $_SESSION['msg'] =  "<p>Faça o login!</p>";
    header("Location: index.php");
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8"/>
    <title>Cadastro de livro</title>
	<link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="estilo.css" media="screen" />
  </head>
  <body>
  <header>
     <nav>
	 <figure>
             <a href="biblioteca.php"><img alt="Logo" src="img/logo.png"></a>
	 </figure>
         <div class="dropdown">
             <button class="dropbtn"><?php echo $_SESSION['nome'] . " &#9787"; ?></button>
                 <div class="dropdown-content">
                     <a href="edit-user.php">Editar perfil</a>
                     <a href="logout.php">Logout</a>
                 </div>
         </div>
     </nav>
  </header>
  <main>
    <div class="container">
        <div class="container-content">
        <h1>Insira os dados do livro!</h1>
        <form id="cadastro" action="process-livro.php" method="post">
	          <label for="nome">Nome</label><br>
                  <input type="text" name="nome" id="nome" required="required" placeholder="On The Road" /><br>
	          <label for="autor">Autor</label><br>
		  <input type="text" name="autor" id="autor" required="required" placeholder="Jack Kerouac" /><br>
                  <label for="ano">Ano</label><br>
		  <input type="text" name="ano" id="ano" required="required" placeholder="1955" /><br>
		  <label for="paginas">Páginas</label><br>
		  <input type="text" name="paginas" id="paginas" required="required" placeholder="566" /><br>
                  <label for="editora">Editora</label><br>
		  <input type="text" name="editora" id="editora" required="required" placeholder="L&PM" /><br>
		  <input type="reset" name="limpar" id="limpar" value="Limpar" />
		  <input type="submit" name="cadastrar" value="Cadastrar" /><br>
        </form>
	  </div>
	</div>  
  </main>
  </body>
</html>