-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 01-Dez-2020 às 03:05
-- Versão do servidor: 10.4.16-MariaDB
-- versão do PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `biblioteca`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `nome` varchar(220) NOT NULL,
  `autor` varchar(220) NOT NULL,
  `ano` varchar(4) NOT NULL,
  `paginas` varchar(4) NOT NULL,
  `editora` varchar(220) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `books`
--

INSERT INTO `books` (`id`, `nome`, `autor`, `ano`, `paginas`, `editora`) VALUES
(2, 'O Capote', 'Nicolai Gogol', '1832', '299', 'L&PM'),
(3, 'Burry my Heart at Wounded Knee', 'Dee Brown', '1970', '389', 'L&PM'),
(8, 'On The Road', 'Jack Kerouac', '1955', '566', 'L&PM');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `nome` varchar(220) NOT NULL,
  `email` varchar(220) NOT NULL,
  `senha` varchar(220) NOT NULL,
  `telefone` varchar(11) NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `estado` varchar(220) NOT NULL,
  `cidade` varchar(220) NOT NULL,
  `rua` varchar(220) NOT NULL,
  `numero` int(6) NOT NULL,
  `comp` varchar(220) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`ID`, `nome`, `email`, `senha`, `telefone`, `cpf`, `estado`, `cidade`, `rua`, `numero`, `comp`) VALUES
(19, 'Michael', 'michael@gmail.com', '$2y$10$x6CfWpuHmRvCsws6xg7Y8.yZUtR6dYkg34sxugaWsNnt097L3tuoe', '00000000000', '00000000000', 'Paraná', 'Curitiba', 'Rua abc', 0, '00'),
(20, 'Vini', 'vini@gmail.com', '$2y$10$xtSvBDulbDPs7aQZ.k5cze1mtLfEY5aGMifgcXt/IRhiVfrJce4u.', '00000000000', '00000000000', 'Paraná', 'Curitiba', 'Rua abc', 0, '00');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
